#This script uses Habit data ONLY



setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
setwd("./Data")

library(tidyverse)



##############use this to obtain data for the first time################
#bioData <- read.csv("https://www.waterqualitydata.us/data/Result/search?organization=21NJDEP1&organization=21NJDEP1_WQX&organization=NJDEP_BFBM&mimeType=csv&zip=no&dataProfile=biological&providers=NWIS&providers=STEWARDS&providers=STORET") 
#metricData <- read.csv("https://www.waterqualitydata.us/data/BiologicalMetric/search?organization=21NJDEP1&organization=21NJDEP1_WQX&organization=NJDEP_BFBM&mimeType=csv&zip=no&providers=NWIS&providers=STEWARDS&providers=STORET") 

#write.csv(bioData,"biologicalresult.csv")
#write.csv(metricData,"biologicalmetric.csv")

###########After first time, start here##########
bioData <- read.csv("biologicalresult.csv",header=T) #data obtained on 1/3/2022, all raw data is here
metricData <- read.csv("biologicalmetric.csv",header=T) #data obtained 1/3/2022, insect count data is split here
ffgData <- read.csv("FFG.csv",header=T) #genera with FFG, Habits, genus and families of all found insects

bioData1 <- bioData




#tidy up the metric data to make it easier to work with, relocating and renaming columns as needed
metricData <- metricData %>% mutate(IndexTypeName = 
                                      gsub("Coastal Plain Macroinvertebrate Index","CPMI",IndexTypeName),
                                    IndexTypeName = gsub("High Gradient Macroinvertebrate Index - genera","HGMI",IndexTypeName),
                                    IndexTypeName = gsub("Pinelands Macroinvertebrate Index","PMI",IndexTypeName)) %>%
  mutate_at("MonitoringLocationIdentifier",str_replace, "NJDEP_BFBM-", "") %>%
  filter(str_detect(MonitoringLocationIdentifier,"AN")) %>%
  relocate(MonitoringLocationIdentifier, 1)


#tidy up the bioData by relocating and renaming variables as needed
bioData1 <- bioData %>%   rename("GenusSpecies"="SubjectTaxonomicName") %>% 
  mutate_at("MonitoringLocationIdentifier",str_replace, "NJDEP_BFBM-", "") %>%
  full_join(ffgData, by="GenusSpecies") %>%
  full_join(metricData, by="MonitoringLocationIdentifier") %>%
  relocate(MonitoringLocationIdentifier, 1) %>% 
  relocate(ActivityStartDate, .after = 1) %>%
  relocate(ActivityLocation.LatitudeMeasure,.after = 2) %>%
  relocate(ActivityLocation.LongitudeMeasure,.after = 3) %>%
  filter(str_detect(MonitoringLocationIdentifier,"AN")) %>%
  relocate("Family", .after = 4) %>%
  relocate("Habit", .after = 5) %>% 
  relocate("FFG", .after = 6) %>% 
  relocate("Genus", .after = 7) %>% 
  relocate(ResultMeasureValue, .after = 8) %>%
  relocate(CharacteristicName, .after = 9) %>% 
  relocate(IndexTypeName,.after = 10)

bioData1 <- bioData1[bioData1$CharacteristicName == "Count",]

vec <- bioData1$IndexTypeName=="CPMI" | bioData1$IndexTypeName=="PMI" 

bioData1 <- bioData1[vec==T,]

insectData <- bioData1

insectData$Habit[insectData$Habit == ""] <- NA
insectData$FFG[insectData$FFG == ""] <- NA

insectData$ResultMeasureValue[is.na(insectData$ResultMeasureValue)] <- 0


insectData$ResultMeasureValue <- as.numeric(insectData$ResultMeasureValue) 




habSum <- insectData %>% group_by(MonitoringLocationIdentifier,
                                         ActivityStartDate,IndexTypeName,
                                         ActivityLocation.LatitudeMeasure,
                                         ActivityLocation.LongitudeMeasure,
                                         Habit) %>% 
  summarise(sum(ResultMeasureValue,na.rm=T)) 
habSum <- habSum[-2]


ffgSum <- insectData %>% group_by(MonitoringLocationIdentifier,
                                  ActivityStartDate,IndexTypeName,
                                  ActivityLocation.LatitudeMeasure,
                                  ActivityLocation.LongitudeMeasure,
                                  FFG) %>% 
  summarise(sum(ResultMeasureValue,na.rm=T)) 
ffgSum <- ffgSum[-2]

famSum <- insectData %>% group_by(MonitoringLocationIdentifier,
                                  ActivityStartDate,IndexTypeName,
                                  ActivityLocation.LatitudeMeasure,
                                  ActivityLocation.LongitudeMeasure,
                                  Family) %>% 
  summarise(sum(ResultMeasureValue,na.rm=T)) 
famSum <- famSum[-2]

genSum <- insectData %>% group_by(MonitoringLocationIdentifier,
                                  ActivityStartDate,IndexTypeName,
                                  ActivityLocation.LatitudeMeasure,
                                  ActivityLocation.LongitudeMeasure,
                                  Genus) %>% 
  summarise(sum(ResultMeasureValue,na.rm=T))
genSum <- genSum[-2]

############################Means############

habMean <- pivot_wider(habSum, 
                                values_from = `sum(ResultMeasureValue, na.rm = T)`,
                                names_from = Habit,values_fn = mean) 
habMean<-habMean[-385,]
habMean<-habMean[-c(10,12)]
habMean[is.na(habMean)] <- 0


ffgMean <- pivot_wider(ffgSum, 
                       values_from = `sum(ResultMeasureValue, na.rm = T)`,
                       names_from = FFG,values_fn = mean)
ffgMean<-ffgMean[-385,]
ffgMean<-ffgMean[-c(9)]
ffgMean[is.na(ffgMean)] <- 0


famMean <- pivot_wider(famSum, 
                       values_from = `sum(ResultMeasureValue, na.rm = T)`,
                       names_from = Family,values_fn = mean)
famMean<-famMean[-385,]
famMean<-famMean[-c(15)]
famMean[is.na(famMean)] <- 0

genMean <- pivot_wider(genSum, 
                       values_from = `sum(ResultMeasureValue, na.rm = T)`,
                       names_from = Genus,values_fn = mean)
genMean<-genMean[-385,]
genMean<-genMean[-c(24)]
genMean[is.na(genMean)] <- 0


setwd("../Output")
write.csv(insectData,"southernBioticdata.csv")

#rm(insectDataHab)
#rm(insectData)

#rm(metricData)

setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

##########GIS Time############

library(ggmap)
library(viridis)
library(spatialEco)
library(sp)
library(skmeans)
library(rgdal)
setwd("./PinelandCut/")

pineland <- readOGR(dsn=getwd(), layer="pinelands", stringsAsFactors = F)

setwd("../Maps/")


southNJbasemap <- 
  get_stamenmap(bbox = c(left = -76.2177, 
                         bottom = 38.8 , 
                         right = -73.3185, 
                         top =40.5390),
                maptype = "terrain-background") 



#set.seed(2)

#########Family Cluster########


test = skmeans(scale(famMean[5:125]), 2)

famMean$cluster = as.factor(test[["cluster"]])

data1 = famMean[famMean$cluster==1,]
data2 = famMean[famMean$cluster==2,]
#data3 = insectDataMeanWideFam[insectDataMeanWideFam$cluster==3,]



test1 = nni(SpatialPoints((data1[3:4])))
test2 = nni(SpatialPoints((data2[3:4])))
#test3 = nni(SpatialPoints((data3[3:4])))



test1$p
test2$p
#test3$p

#nni(SpatialPoints(unique(insectHabrescaled[5:4])))


ggmap(southNJbasemap) + 
  geom_polygon(data = pineland, 
               aes( x = long, y = lat, group = group),color="black",fill="green",alpha=.25) +
  geom_point(data = famMean,  
                                   aes(x=ActivityLocation.LongitudeMeasure,
                                       y=ActivityLocation.LatitudeMeasure,color=cluster),
                                   size=.5) +
  scale_color_viridis(discrete=T, name="Cluster",
                      labels = c(paste(1, "(p =",round(test1$p,3),")"),
                                 paste(2, "(p =",round(test2$p,3),")")),
                      option = "magma",direction=1,end = .5) + 
  coord_equal() + coord_map() +
  ggtitle("Biotic - Family")+ 
  theme_void() + theme(text = element_text(size = 20))+ 
  guides(colour = guide_legend(override.aes = list(size=5)))

ggsave("Biotic - Family.png",width = 1920, height = 1080, units="px")


###########Habit Cluster##########


test = skmeans(scale(habMean[5:10]), 2)

habMean$cluster = as.factor(test[["cluster"]])

data1 = habMean[habMean$cluster==1,]
data2 = habMean[habMean$cluster==2,]
#data3 = insectDataMeanWideFam[insectDataMeanWideFam$cluster==3,]



test1 = nni(SpatialPoints((data1[3:4])))
test2 = nni(SpatialPoints((data2[3:4])))
#test3 = nni(SpatialPoints((data3[3:4])))



test1$p
test2$p
#test3$p

#nni(SpatialPoints(unique(insectHabrescaled[5:4])))


ggmap(southNJbasemap) + 
  geom_polygon(data = pineland, 
               aes( x = long, y = lat, group = group),color="black",fill="green",alpha=.25) +
  geom_point(data = habMean, 
                                   aes(x=ActivityLocation.LongitudeMeasure,
                                       y=ActivityLocation.LatitudeMeasure,color=cluster),
                                   size=.5) +
  scale_color_viridis(discrete=T, name="Cluster",
                      labels = c(paste(1, "(p =",round(test1$p,3),")"),
                                 paste(2, "(p =",round(test2$p,3),")")),
                      option = "magma",direction=-1,end = .5) + 
  ggtitle("Biotic - Habit")+ 
  theme_void() + theme(text = element_text(size = 20))+ 
  guides(colour = guide_legend(override.aes = list(size=5)))

ggsave("Biotic - Habit.png",width = 1920, height = 1080,units="px")


#########FFG Cluster########


test = skmeans(scale(ffgMean[5:11]), 2)

ffgMean$cluster = as.factor(test[["cluster"]])

data1 = ffgMean[ffgMean$cluster==1,]
data2 = ffgMean[ffgMean$cluster==2,]
#data3 = insectDataMeanWideFam[insectDataMeanWideFam$cluster==3,]



test1 = nni(SpatialPoints((data1[3:4])))
test2 = nni(SpatialPoints((data2[3:4])))
#test3 = nni(SpatialPoints((data3[3:4])))



test1$p
test2$p
#test3$p

#nni(SpatialPoints(unique(insectHabrescaled[5:4])))


ggmap(southNJbasemap) + 
  geom_polygon(data = pineland, 
               aes( x = long, y = lat, group = group),color="black",fill="green",alpha=.25) +
  geom_point(data = ffgMean, 
                                   aes(x=ActivityLocation.LongitudeMeasure,
                                       y=ActivityLocation.LatitudeMeasure,color=cluster),
                                   size=.5) +
  scale_color_viridis(discrete=T, name="Cluster",
                      labels = c(paste(1, "(p =",round(test1$p,3),")"),
                                 paste(2, "(p =",round(test2$p,3),")")),
                      option = "magma",direction=1,end = .5) + 
  ggtitle("Biotic - FFG")+ 
  theme_void() + theme(text = element_text(size = 20))+ 
  guides(colour = guide_legend(override.aes = list(size=5)))

ggsave("Biotic - FFG.png",width = 1920, height = 1080,units="px")


#########Genus Cluster########


test = skmeans(scale(genMean[5:434]), 2)

genMean$cluster = as.factor(test[["cluster"]])

data1 = genMean[genMean$cluster==1,]
data2 = genMean[genMean$cluster==2,]
#data3 = insectDataMeanWideFam[insectDataMeanWideFam$cluster==3,]



test1 = nni(SpatialPoints((data1[3:4])))
test2 = nni(SpatialPoints((data2[3:4])))
#test3 = nni(SpatialPoints((data3[3:4])))



test1$p
test2$p
#test3$p

#nni(SpatialPoints(unique(insectHabrescaled[5:4])))


ggmap(southNJbasemap) + 
  geom_polygon(data = pineland, 
               aes( x = long, y = lat, group = group,fill="PMI"),color="black",alpha=.25) +
  geom_point(data = genMean, aes(x=ActivityLocation.LongitudeMeasure,
                                       y=ActivityLocation.LatitudeMeasure,color=cluster),
                                   size=.5) +
  scale_color_viridis(discrete=T, name="Cluster",
                      labels = c(paste(1, "(p =",round(test1$p,3),")"),
                                 paste(2, "(p =",round(test2$p,3),")")),
                      option = "magma",direction=1,end = .5) + 
  scale_fill_viridis(discrete=T, name="PMI Border",
                     direction=1,option="D",begin = .2)+
  ggtitle("Genera Cluster")+ 
  theme_void() + theme(text = element_text(size = 20))+ 
  guides(colour = guide_legend(override.aes = list(size=5)))

ggsave("Biotic - Genus.png",width = 1920, height = 1080,units="px")





#########Biotic (Genus) - allCluster########
bioticMean<-cbind(ffgMean[1:11],habMean[5:10],genMean[5:434])



test = skmeans(scale(bioticMean[5:447]), 2)

bioticMean$cluster = as.factor(test[["cluster"]])

data1 = bioticMean[bioticMean$cluster==1,]
data2 = bioticMean[bioticMean$cluster==2,]
#data3 = bioticMean[bioticMean$cluster==3,]



test1 = nni(SpatialPoints((data1[3:4])))
test2 = nni(SpatialPoints((data2[3:4])))
#test3 = nni(SpatialPoints((data3[3:4])))



test1$p
test2$p
#test3$p

#nni(SpatialPoints(unique(insectHabrescaled[5:4])))


ggmap(southNJbasemap) + 
  geom_polygon(data = pineland, 
               aes( x = long, y = lat, group = group,fill="PMI"),color="black",alpha=.25) +
  geom_point(data = bioticMean, 
                                   aes(x=ActivityLocation.LongitudeMeasure,
                                       y=ActivityLocation.LatitudeMeasure,color=cluster),
                                   size=.5) +
  scale_color_viridis(discrete=T, name="Cluster",
                      labels = c(paste(1, "(p =",round(test1$p,3),")"),
                                 paste(2, "(p =",round(test2$p,3),")")),
                      option = "magma",direction=-1,end = .5) + 
  scale_fill_viridis(discrete=T, name="PMI Border",
                      direction=1,option="D",begin = .2)+
  ggtitle("Genera Cluster")+ 
  theme_void() + theme(text = element_text(size = 20))+ 
  guides(colour = guide_legend(override.aes = list(size=5)))

ggsave("Biotic - All (Genus).png",width = 1920, height = 1080,units="px")




#########Biotic (Fam) Cluster########
bioticMean<-cbind(ffgMean[1:11],habMean[5:10],famMean[5:125])



test = skmeans(scale(bioticMean[5:138]), 2)

bioticMean$cluster = as.factor(test[["cluster"]])

data1 = bioticMean[bioticMean$cluster==1,]
data2 = bioticMean[bioticMean$cluster==2,]
#data3 = insectDataMeanWideFam[insectDataMeanWideFam$cluster==3,]



test1 = nni(SpatialPoints((data1[3:4])))
test2 = nni(SpatialPoints((data2[3:4])))
#test3 = nni(SpatialPoints((data3[3:4])))



test1$p
test2$p
#test3$p

#nni(SpatialPoints(unique(insectHabrescaled[5:4])))


ggmap(southNJbasemap) + 
  geom_polygon(data = pineland, 
               aes( x = long, y = lat, group = group),color="black",fill="green",alpha=.25) +
  geom_point(data = bioticMean, 
                                   aes(x=ActivityLocation.LongitudeMeasure,
                                       y=ActivityLocation.LatitudeMeasure,color=cluster),
                                   size=.5) +
  scale_color_viridis(discrete=T, name="Cluster",
                      labels = c(paste(1, "(p =",round(test1$p,3),")"),
                                 paste(2, "(p =",round(test2$p,3),")")),
                      option = "magma",direction=-1,end = .5) + 
  ggtitle("Biotic - All(Family)")+ 
  theme_void() + theme(text = element_text(size = 20))+ 
  guides(colour = guide_legend(override.aes = list(size=5)))

ggsave("Biotic - All (Family).png",width = 1920, height = 1080,units="px")

write.csv(data1,"data1bio.csv")
write.csv(data2,"data2bio.csv")